# This file is a template, and might need editing before it works on your project.
FROM debian:stable

WORKDIR /home/
ADD uls/ /home/
RUN mkdir /home/gt_app && mkdir /home/gt_app/callsigns
RUN touch /home/auth.json
RUN apt-get update && apt-get install -qq mariadb-server cron curl gnupg php php-mysql wget unzip sqlite3
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-cli -y
      
RUN /etc/init.d/mariadb start && mysql -uroot --password="" -e "source uls.sql"
ADD uls/uls-cron /etc/cron.d/uls-cron
RUN chmod 0644 /etc/cron.d/uls-cron && crontab /etc/cron.d/uls-cron && touch /var/log/uls.log

CMD gcloud auth activate-service-account --key-file=/home/auth/cent7-288417-ec0cfbfda018.json && /etc/init.d/mariadb start && cron && tail -f /var/log/uls.log

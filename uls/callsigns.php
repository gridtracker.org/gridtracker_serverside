<?php ini_set("memory_limit", "-1"); ?>
<?php

$path = "gt_app/callsigns/";

$mysqli = new mysqli('127.0.0.1', 'uls', 'foxcharliecharlie', 'ULSDATA');

if ($mysqli->connect_errno) {
    echo "Sorry, callsigns failed.";
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

$sql = 'select PUBACC_EN.call_sign, PUBACC_EN.zip_code,PUBACC_EN.state from PUBACC_HD,PUBACC_EN where PUBACC_HD.unique_system_identifier = PUBACC_EN.unique_system_identifier and PUBACC_HD.license_status = "A" and PUBACC_EN.zip_code != ""  order by PUBACC_EN.call_sign;';
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, callsigns failed.";
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

if ($result->num_rows === 0) {
    echo "We could not find a match for ID , sorry about that. Please try again.";
    exit;
}
$callsj = [];
$calls = [];
    while ($row = $result->fetch_row()) {
       $row[1] = substr($row[1],0,5);
	$callsj[$row[0]] = strtoupper($row[2]).$row[1];
       $row[1] .= strtoupper($row[2]).$row[0];
       $calls[] = $row[1]; 
    }

$now = time();

$manifest = Array();
$manifest["ts"] = $now;
$manifest["calls"] = $calls;

$data = "";

foreach( $calls as $value )
{
	$data .= $value."\n";
}

$fn = "tmp/callsigns.txt";
file_put_contents($fn, $data);

$fn = "gt_app/callsigns/callsigns.txt";
file_put_contents($fn, $data);

$manifest["size"] = strlen($data);
$manifest["calls"] = count($calls);
$fn = $path."callsigns_ts.json";
file_put_contents($fn, json_encode($manifest));

$result->free();
$mysqli->close();
?>

<?php ini_set("memory_limit", "-1"); ?>

<?php
// This script takes the CSV file from https://ic.gc.ca/eic/site/025.nsf/eng/h_00004.html,
// and outputs a txt file in the format of PostalPVCallsign
// This script is Copyright (C) 2020 Matthew Chambers NR0Q
// NOT FOR DISTRIBUTION without prior written consent from the author!

$csvData = file_get_contents($argv[1]);
$lines = explode(PHP_EOL, $csvData);
$array = array();
foreach ($lines as $line) {
    $array[] = str_getcsv($line,";");
}
array_shift($array);
array_pop($array);
foreach ($array as $row)
{
	$postal = strlen($row[6])?$row[6]: (strlen($row[17])?$row[17]:"XXXXXX");
	$prov =  strlen($row[5])?$row[5]: (strlen($row[16])?$row[16]:"");
	if ( strlen($prov) )
		echo $postal . $prov . $row[0] . "\n";

}
?>

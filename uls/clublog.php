<?php ini_set("memory_limit", "-1"); ?>

<?php

$zip = file_get_contents("https://cdn.clublog.org/clublog-users.json.zip");

if ( $zip && strlen($zip) )
{
	file_put_contents("/tmp/json.zip", $zip);
	system("unzip /tmp/json.zip -d /tmp");
	unlink("/tmp/json.zip");
}

$json = file_get_contents("/tmp/clublog_users.json");
unlink("/tmp/clublog_users.json");

$calls = Array();

if ( $json && strlen($json) )
{
	$data = json_decode($json);
	foreach ($data as $key => $value)
	{
		if ( isset($value->oqrs) &&  $value->oqrs == 1 )
			$calls[$key] = 1;
	}
}

$fn = "tmp/clublog.json";
file_put_contents($fn, json_encode($calls));

$fn = "gt_app/callsigns/clublog.json";
file_put_contents($fn, json_encode($calls));

?>

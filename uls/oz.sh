#!/bin/bash
set -ex
die() { echo "$*" 1>&2 ; exit 1; }

# Create a temporary working directory and cd into it
TMPDIR=`mktemp -d`
test -n "$TMPDIR" || die "Failed to create temporary directory"
pushd $TMPDIR

# Fetch the data and unzip
wget https://web.acma.gov.au/rrl-updates/spectra_rrl.zip
unzip spectra_rrl.zip

# Make sure we have the files we need
test -f licence.csv || die "Can't find licence.csv in zip"
test -f device_details.csv || die "Can't find device_details.csv in zip"
test -f client.csv || die "Can't find client.csv in zip"

# Use sqlite to load the data, do the join and spit out the reuslt
sqlite3 > /home/gt_app/aus.txt << EOF
.mode csv
.import licence.csv LICENCE
.import device_details.csv DEVICE_DETAILS
.import client.csv CLIENT
select c.POSTAL_STATE, dd.CALL_SIGN from CLIENT c join LICENCE l ON c.CLIENT_NO = l.CLIENT_NO join DEVICE_DETAILS dd on l.LICENCE_NO = dd.LICENCE_NO where l.SV_ID = "6" and l.STATUS_TEXT = "Granted" and c.POSTAL_STATE != "" and dd.CALL_SIGN != "";
EOF
popd
rm -fr $TMPDIR

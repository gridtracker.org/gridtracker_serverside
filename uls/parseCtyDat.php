<?php

error_reporting(E_ERROR | E_PARSE);

$yesterdaySec = time() - (86400);
$yesterdayArray = getdate($yesterdaySec);

$year = $yesterdayArray["year"];
$month = str_pad($yesterdayArray["mon"], 2, '0', STR_PAD_LEFT);
$day = str_pad($yesterdayArray["mday"], 2, '0', STR_PAD_LEFT);
$date = $year.$month.$day;
$filename = "bigcty-".$date.".zip";
$tmpName = "/tmp/".$filename;
$datName = "/tmp/cty.csv";
$jsonName = "/tmp/ctydat.json";
$jsonVerName = "/tmp/ctydatver.json";
$publishCommand = "gsutil -m cp ".$jsonName." gs://gt_app/";
$publishVersion = "gsutil -m cp ".$jsonVerName." gs://gt_app/";
// pre-clean
unlink($tmpName);
unlink($datName);

if(@copy("https://www.country-files.com/bigcty/download/".$year."/".$filename, $tmpName))
{
    $command = "unzip ".$tmpName." \"cty.csv\" -d /tmp";
    system($command);

    if (file_exists($datName))
    {
        $csvs = array_map('str_getcsv', file($datName));
        $datas = [];
        $column_names = ["pp","name","dxcc","continent","cqzone","ituzone","lat","lon","timezone","prefix"];
        foreach ($csvs as $key => $csv) {
            $dxcc = $csv[2];
            if (substr($csv[0], 0, 1) != '*')
            {
                foreach ($column_names as $column_key => $column_name)
                {
                    if ($column_name == "dxcc") {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    }
                    if ($column_name == "pp") {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    } else if ($column_name == "name") {
                        // skip
                    } else if ($column_name == "continent") {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                        // skip
                    } else if ($column_name == "cqzone") {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    } else if ($column_name == "ituzone") {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    } else if ($column_name == "lat") {
                        // skip
                    } else if ($column_name == "lon") {
                        // skip
                    } else if ($column_name == "timezone") {
                        // skip
                    } else if ($column_name == "prefix") {
                        // $alias_dirty = explode(" ", $csv[$column_key]);
                        // $alias_clean = array_map('cleanAlias', $alias_dirty);
                        // $datas[$dxcc][$column_name] = $alias_clean;
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    } else {
                        $datas[$dxcc][$column_name] = $csv[$column_key];
                    }
                }
            }
        }
        $json = json_encode($datas, JSON_NUMERIC_CHECK);
        file_put_contents($jsonName, $json);
        system($publishCommand);
        unlink($jsonName);

        $verdata = [];
        $verdata["version"] = $date;
        $json = json_encode($verdata, JSON_NUMERIC_CHECK);
        file_put_contents($jsonVerName, $json);
        system($publishVersion);
        unlink($jsonVerName);

        echo "Updated: ".$date."\r\n";
    }
    else
    {
        echo "No update, missing: ".$datName."\r\n";
    }
    unlink($datName);
    unlink($tmpName);
}
else
{
    echo "No update avaialble for yesterday: ".$date."\r\n";
}

function cleanAlias($alias)
{
    $val = rtrim($alias,";");
    $val = explode("(", $val)[0];
    $val = explode("[", $val)[0];
    $val = explode("<", $val)[0];
    $val = explode("{", $val)[0];
    $val = explode("~", $val)[0];
    return $val;
}
?>
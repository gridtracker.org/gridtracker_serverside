<?php
$output = [];
$output["parks"] = [];
$output["locations"] = [];

$file = "all_parks_ext.csv";

if (($handle = fopen($file, "r")) !== FALSE) {
    $csvs = [];
    while(! feof($handle)) {
       $csvs[] = fgetcsv($handle);
    }
    $datas = [];
    $column_names = [];
    foreach ($csvs[0] as $single_csv) {
        $column_names[] = $single_csv;
    }
    foreach ($csvs as $key => $csv) {
        if ($key === 0) {
            continue;
        }
        $ref = $csv[0];
        //echo $ref . "\n";

        if ($ref != "") {
            foreach ($column_names as $column_key => $column_name) {
                if ($column_name == "reference") {
                    // skip
                } else {
                    $datas[$ref][$column_name] = $csv[$column_key];
                }
            }
        }
    }
    $output["parks"] = $datas;
    fclose($handle);
}

$locations_json = file_get_contents("https://api.pota.app/locations");
if ($locations_json)
{
  $data = json_decode($locations_json);
  $objects = [];
  foreach ($data as $value ) {
    $objects[$value->locationDesc] = trim($value->locationName);
  }
  $output["locations"] = $objects;
}

$final = json_encode($output);
echo $final;

?>

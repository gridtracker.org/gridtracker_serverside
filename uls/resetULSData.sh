#!/bin/bash

# Info
#
# before running this script, create a database named ULSDATA on your mysql host.
#
## TODO write script to check if mysql is config'd if not config it and import uls.sql
#
# edit your .my.cnf to include your mysql host, username, password (see below)
#
# [client]
host="127.0.0.1"
user="uls"
password="foxcharliecharlie"
#

## make sure we are where we think we are // not needed in docker image
cd /home/ || exit

#### Get new FCC data
if test -f l_amat.zip; then
    mv l_amat.zip old_l_amat.zip
fi
echo ""
echo "getting new data from FCC ULS"
echo ""
wget -q ftp://wirelessftp.fcc.gov/pub/uls/complete/l_amat.zip
echo ""
echo "expanding data"
echo ""
unzip l_amat.zip

chmod 755 ./*.dat

#### Clean up old data from Database
echo ""
echo "clearing old data out of database"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_AM" ULSDATA
echo "PUBACC_AM truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_CO" ULSDATA
echo "PUBACC_CO truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_EN" ULSDATA
echo "PUBACC_EN truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_HD" ULSDATA
echo "PUBACC_HD truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_HS" ULSDATA
echo "PUBACC_HS truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_LA" ULSDATA
echo "PUBACC_LA truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_SC" ULSDATA
echo "PUBACC_SC truncated"
mysql --user="${user}" --password="${password}" --host="${host}" --execute="truncate table PUBACC_SF" ULSDATA
echo "PUBACC_SF truncated"

#### import the new data
echo ""
echo "converting shit data to sql"
php work.php
rm EN.dat
mv OUT.dat EN.dat

chmod 777 EN.dat
chmod 777 HD.dat

echo ""
echo "importing new data"
mysql --user="${user}" --password="${password}" --host="${host}" ULSDATA < load.sql
echo "... done"

#### Remove old FCC data files.

echo ""
echo "cleaning up"
rm -f ./*.dat counts

#### We won't need this anymore, lets stop updating it.
#### echo ""
#### echo "Generating state json"
#### php states.php

echo ""
echo "Generating Callsign Txt"
php callsigns.php

echo ""
echo "Generating OQRS"
php clublog.php

echo ""
echo "Importing IOTA"
bash iota.sh

echo ""
echo "We Do Canada!"
wget -q https://apc-cap.ic.gc.ca/datafiles/amateur_delim.zip
unzip amateur_delim.zip
rm amateur_delim.zip
rm readme_amat_delim.txt
rm lisezmoi_amat_delim.txt
php canada.php amateur_delim.txt > canada.txt
rm amateur_delim.txt
mv canada.txt gt_app/canada.txt

echo ""
echo "Uploading data to bucket"
cd gt_app || exit
gsutil -m cp -r ./ gs://gt_app/
## gsutil -m cp -r ./ gs://app.gridtracker.org/


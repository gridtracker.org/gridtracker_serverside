#!/bin/bash

mkdir -p gt_app/callsigns
mkdir -p tmp

# Run updateULSData.sh
bash updateULSData.sh

# Run callsigns.php
php callsigns.php

# Canada
wget -q https://apc-cap.ic.gc.ca/datafiles/amateur_delim.zip
unzip amateur_delim.zip
rm amateur_delim.zip
rm readme_amat_delim.txt
rm lisezmoi_amat_delim.txt
php canada.php amateur_delim.txt > canada.txt
rm amateur_delim.txt
mv canada.txt gt_app/canada.txt

# Run iota.sh
bash iota.sh

# Run updateULSData.sh
bash updatePota.sh

# Run clublog.php
php clublog.php

# Run ParseCtyDat
bash ctyDat.sh

#upload gt_app
cd gt_app || exit
gsutil -m cp -r ./ gs://gt_app/



<?php ini_set("memory_limit", "-1"); ?>
<?php

$path = "gt_app/callsigns/";

$mysqli = new mysqli('127.0.0.1', 'uls', 'foxcharliecharlie', 'ULSDATA');

if ($mysqli->connect_errno) {
    echo "Sorry, states failed.";
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

$sql = 'select PUBACC_EN.state, PUBACC_HD.call_sign from PUBACC_HD,PUBACC_EN where PUBACC_HD.unique_system_identifier = PUBACC_EN.unique_system_identifier and PUBACC_HD.license_status = "A" order by PUBACC_EN.state;';
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, states failed.";
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

if ($result->num_rows === 0) {
    echo "We could not find a match for ID , sorry about that. Please try again.";
    exit;
}

$states = Array();
    while ($row = $result->fetch_assoc()) {
        $states[strtoupper($row["state"])][] = $row["call_sign"];
    }

$now = time();

$manifest = Array();
$manifest["ts"] = $now;
$manifest["cnt"] = Array();
foreach ( $states as $state => $data )
{
	//echo $state."\r\n";
	if ( $state != "" && $state != 94)
	{
		sort($data);
		$js = Array();
		$js["ts"] = $now;
		$js["c"] = $data;
		$fn = $path.$state.".callsigns.json";
		file_put_contents($fn, json_encode($js));
		$manifest["cnt"][$state] = count($data);
	}
}


$fn = $path."manifest.json";
file_put_contents($fn, json_encode($manifest));

$result->free();
$mysqli->close();
?>

#!/bin/bash

## remove old file
rm all_parks_ext.csv

## get new pota data
echo "Getting latest pota data..."
wget http://pota.app/all_parks_ext.csv

## making a pota file
echo "Getting data from POTA and turning into JSON..."
php pota.php > pota.json

echo ""
echo "Uploading data to bucket..."
gsutil -m cp pota.json gs://gt_app/
## gsutil -m cp pota.json gs://app.gridtracker.org/

#!/bin/bash

# Info
#
# before running this script, create a database named ULSDATA on your mysql host.
#
# edit your .my.cnf to include your mysql host, username, password (see below)
#
# [client]
host=127.0.0.1
user=uls
password=foxcharliecharlie
#


yesterday=$(date -d "1 day ago" +%a)
day=${yesterday,,}

#### Get new FCC data

echo ""
echo "Getting yesterday's data from FCC ULS"
echo ""
wget -q ftp://wirelessftp.fcc.gov/pub/uls/daily/l_am_"$day".zip

echo ""
echo "expanding data"
echo ""
unzip l_am_"$day".zip
chmod 755 *.dat
#### import the new data

echo ""
echo "importing new data"
echo ""

#### import the new data
echo ""
echo "converting shit data to sql"
php work.php
rm EN.dat
mv OUT.dat EN.dat

chmod 777 EN.dat
chmod 777 HD.dat

echo ""
echo "importing new data"
mysql --user="${user}" --password="${password}" --host="${host}" ULSDATA < load.sql
echo "... done"

echo ""
echo "done importing this batch"

#### Remove old FCC data files.

echo ""
echo "cleaning up"
echo ""
rm -f l_am_"$day".zip *.dat counts
echo "files removed"
echo ""

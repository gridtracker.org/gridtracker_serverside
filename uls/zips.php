<?php ini_set("memory_limit", "-1"); ?>

<?php
$path = "gt_app/callsigns/";

$mysqli = new mysqli('127.0.0.1', 'uls', 'foxcharliecharlie', 'ULSDATA');

if ($mysqli->connect_errno) {
    echo "Sorry, zip codes.";
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

$sql = 'select zip, state, county from zipcodes;';
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, zip codes failed.";
    echo "Error: Our query failed to execute and here is why: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

if ($result->num_rows === 0) {
    echo "We could not find a match for ID $aid, sorry about that. Please try again.";
    exit;
}

$states = Array();
$countys = Array();

    while ($row = $result->fetch_assoc()) {

	$row["zip"] = str_pad($row["zip"], 5, '0', STR_PAD_LEFT);
        $states[strtoupper($row["state"])][$row["county"]][] = $row["zip"];
    }

$now = time();

$data = json_encode($states,JSON_PRETTY_PRINT);
echo $data;


$result->free();
$mysqli->close();
?>
